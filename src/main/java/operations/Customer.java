package operations;

import java.util.SortedMap;
import java.util.TreeMap;

public class Customer {
	String name;
	int discount;
	SortedMap<String, Integer> scoredProducts = new TreeMap<>();
	int prodScoredN;
	int totalProducts;
	int largestExpense;

	public Customer(String name, int discount) {
		this.name = name;
		this.discount = discount;
		this.scoredProducts = new TreeMap<String, Integer>();
		this.prodScoredN = 0;
		this.totalProducts = 0;
		this.largestExpense = 0;
	}

	public String getName() {
		return this.name;
	}

	public void addDiscount(int discount) {
		this.discount += discount;
	}

	public int getDiscount() {
		return this.discount;
	}

	public void addProduct(String product, int quantity) {
		if (!this.scoredProducts.containsKey(product))
			this.scoredProducts.put(product, -1);
		this.totalProducts += quantity;
	}

	public int addScoreForProduct(String product, int score) {
		Integer s = this.scoredProducts.get(product);
		if (s == null || s != -1)
			return -1;
		this.scoredProducts.put(product, score);
		this.prodScoredN++;
		return this.prodScoredN;
	}

	public int getScoreForProduct(String product) {
		Integer score = this.scoredProducts.get(product);
		if (score == null)
			return -1;
		return score;
	}

	public int getTotalProducts() {
		return this.totalProducts;
	}

	public void addLargestExpense(int expense) {
		if (expense > this.largestExpense)
			this.largestExpense = expense;
	}

	public int getLargestExpense() {
		return this.largestExpense;
	}
}
