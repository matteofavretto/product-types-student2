package operations;

public class ProductType {
	String name;
	int availableQuantity;
	int price;
	
	public ProductType(String name, int quantity, int price) {
		this.name = name;
		this.availableQuantity = quantity;
		this.price = price;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAvailableQuantity() {
		return this.availableQuantity;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public void sellProducts(int n) {
		this.availableQuantity -= n;
	}

}
