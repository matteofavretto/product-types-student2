package it.polito.po.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Test;

import operations.*;

public class TestClass {
	
	Operations op = new Operations();
	
	@Test
	public void testAddProducts() throws OException {
		int totalPrice1 = op.addProductType("chairA", 10, 12);
		int totalPrice2 = op.addProductType("deskA", 4, 20);
		int totalPrice3 = op.addProductType("chairB", 10, 15);
		int totalPrice4 = op.addProductType("bedA", 2, 50);
		
		assertEquals(120, totalPrice1);
		assertEquals(80, totalPrice2);
		assertEquals(150, totalPrice3);
		assertEquals(100, totalPrice4);
	}
	
	@Test
	public void testGroupProductsByPrice() throws OException {
		op.addProductType("chairA", 10, 12);
		op.addProductType("deskA", 4, 20);
		op.addProductType("chairB", 10, 12);
		op.addProductType("bedA", 2, 50);
		
		SortedMap<Integer, List<String>> map1 = op.groupingProductTypesByPrices();
		String expected = "{12=[chairA, chairB], 20=[deskA], 50=[bedA]}";
		assertEquals(expected, map1.toString());
	}
	
	@Test
	public void testCustomerOrders() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addDiscount("c1", 20);
		String order = "sofa:2";
		
		int expense = op.customerOrder("c1", order, 10);
		assertEquals(expense, 290);
		
		op.addProductType("chair", 3, 50);
		op.addDiscount("c2", 20);
		String order2 = "chair:2 sofa:1";
		
		int expense2 = op.customerOrder("c2", order2, 20);
		assertEquals(expense2, 230);
	}
	
	@Test(expected=OException.class)
	public void testOrderExceedDiscount() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addDiscount("c1", 20);
		String order = "sofa:12";
		
		int expense = op.customerOrder("c1", order, 30);
	}
	
	@Test
	public void testProductEvaluation() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addDiscount("c1", 20);
		op.addDiscount("c2", 10);
		String order1 = "sofa:2";
		String order2 = "sofa:1";
		
		op.customerOrder("c1", order1, 10);
		op.customerOrder("c2", order2, 10);
		op.evalByCustomer("c1", "sofa", 8);
		op.evalByCustomer("c2", "sofa", 6);
		
		SortedMap<Integer, List<String>> map;
		map = op.groupingCustomersByScores("sofa");
		String expected = "{6=[c2], 8=[c1]}";
		assertEquals(expected, map.toString());
	}
	
	@Test
	public void testGroupingByNumberOfProducts() throws OException {
		op.addProductType("sofa", 10, 150);
		op.addProductType("desk", 5, 100);
		op.addProductType("chair", 2, 50);
		op.addDiscount("c1", 20);
		op.addDiscount("c2", 10);
		op.addDiscount("c3", 30);
		String order1 = "sofa:2 desk:1";
		String order2 = "desk:3 chair:2";
		String order3 = "sofa:5";
		op.customerOrder("c1", order1, 10);
		op.customerOrder("c2", order2, 0);
		op.customerOrder("c3", order3, 20);
		
		SortedMap<Integer, List<String>> map;
		map = op.groupingCustomersByNumberOfProductsPurchased();
		String expected = "{3=[c1], 5=[c2, c3]}";
		assertEquals(expected, map.toString());
	}
}
